/*
 * Copyright (c) 2023. Aeontronix Inc
 */

package com.aeontronix.aeoncli.config;

/**
 * Implementation of {@link ConfigurationService} which stores the configuration
 * as a json file on the filesystem (this requires jackson library)
 */
public class ConfigurationServiceFSImpl implements ConfigurationService {
    @Override
    public Configuration getConfiguration() {
        return null;
    }

    @Override
    public void saveConfiguration() {
    }
}
