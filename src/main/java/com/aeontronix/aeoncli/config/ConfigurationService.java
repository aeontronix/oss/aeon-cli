/*
 * Copyright (c) 2023. Aeontronix Inc
 */

package com.aeontronix.aeoncli.config;

public interface ConfigurationService {
    Configuration getConfiguration();
    void saveConfiguration();
}
