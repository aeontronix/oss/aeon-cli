/*
 * Copyright (c) 2023. Aeontronix Inc
 */

package com.aeontronix.aeoncli.config;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Parameters;

import java.util.concurrent.Callable;

@Command(name = "profile",aliases = "p", description = "Change the active configuration profile")
public class ProfileCmd implements Callable<Integer> {
    @Parameters(description = "Profile name")
    private String profileName;
    @CommandLine.Option(names = {"-c","--create"}, description = "Create the profile if it doesn't exist")
    private boolean create;
    @Override
    public Integer call() throws Exception {
        return 0;
    }
}
