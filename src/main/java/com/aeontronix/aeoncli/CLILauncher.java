/*
 * Copyright (c) Aeontronix 2021
 */

package com.aeontronix.aeoncli;

import picocli.CommandLine;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CLILauncher {
    protected String loggingSupportClass = "com.aeontronix.aeoncli.logging.JCLLogFormatter";
    private final String[] args;
    private final boolean debugEnabled;
    private final CommandLine commandLine;

    public CLILauncher(RootCommand cmd, String... args) {
        this(cmd, null, Arrays.asList("com", "org", "net"), args);
    }

    public CLILauncher(RootCommand cmd, CommandLine.IFactory iFactory, String... args) {
        this(cmd, iFactory, Arrays.asList("com", "org", "net"), args);
    }

    public CLILauncher(RootCommand cmd, CommandLine.IFactory iFactory, List<String> debugPackages, String... args) {
        this.args = args;
        if (iFactory != null) {
            commandLine = new CommandLine(cmd, iFactory);
        } else {
            commandLine = new CommandLine(cmd);
        }
        commandLine.setColorScheme(CommandLine.Help.defaultColorScheme(CommandLine.Help.Ansi.ON));
        commandLine.setUsageHelpAutoWidth(true);
        commandLine.setCaseInsensitiveEnumValuesAllowed(true);
        commandLine.setPosixClusteredShortOptionsAllowed(false);
        try {
            commandLine.parseArgs(args);
        } catch (Exception e) {
            // ignoring at this stage
        }
        debugEnabled = cmd.isDebugEnabled();
        if (debugEnabled) {
            setupLogging(debugPackages);
            System.out.println("Parameters: " + Arrays.asList(args));
        }
        commandLine.setExecutionExceptionHandler((ex, cl, pr) -> {
            if (debugEnabled) {
                throw ex;
            } else {
                System.err.println("ERROR: " + ex.getMessage());
                return -1;
            }
        });
    }

    protected void setupLogging(List<String> debugPackages) {
        try {
            Class.forName(loggingSupportClass)
                    .getMethod("setupLogging", boolean.class, List.class).invoke(null, debugEnabled, debugPackages);
        } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    public int execute() {
        return commandLine.execute(args);
    }
}
