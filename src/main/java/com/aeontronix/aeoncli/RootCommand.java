/*
 * Copyright (c) 2023. Aeontronix Inc
 */

package com.aeontronix.aeoncli;

public interface RootCommand {
    boolean isDebugEnabled();
}
