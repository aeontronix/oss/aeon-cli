/*
 * Copyright (c) 2023. Aeontronix Inc
 */

package com.aeontronix.aeoncli;

import picocli.CommandLine;

public class AbstractRootCommand implements RootCommand {
    @CommandLine.Option(names = {"--debug","-d"}, description = "Enable debug logging")
    private boolean debug;

    @Override
    public boolean isDebugEnabled() {
        return debug;
    }
}
