/*
 * Copyright (c) 2023. Aeontronix Inc
 */

package com.aeontronix.aeoncli;

import picocli.CommandLine;
import picocli.CommandLine.Command;

import java.util.concurrent.Callable;

@Command(subcommands = {SubCmd.class})
public class MainCmd extends AbstractRootCommand {
}
