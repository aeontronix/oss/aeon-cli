/*
 * Copyright (c) Aeontronix 2022
 */

package com.aeontronix.aeoncli.logging;

import com.aeontronix.aeoncli.CLILogFormatter;
import org.fusesource.jansi.Ansi;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.*;

public class JCLLogFormatter extends Formatter {
    private final DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);
    private final Date date = new Date();
    private boolean showLevel;
    private boolean showTimestamp;
    private String separator = " ";

    @Override
    public String format(final LogRecord record) {
        final StringBuilder txt = new StringBuilder();
        if (showLevel) {
            txt.append(record.getLevel().getLocalizedName()).append(separator);
        }
        if (showTimestamp) {
            synchronized (dateFormat) {
                date.setTime(record.getMillis());
                txt.append(dateFormat.format(date)).append(separator);
            }
        }
        String errorMessage = formatMessage(record);
        if (record.getLevel().intValue() > Level.INFO.intValue()) {
            errorMessage = Ansi.ansi().fgBrightRed().a("ERROR: " + errorMessage).reset().toString();
        }
        txt.append(errorMessage).append('\n');
        return txt.toString();
    }

    public void setDebug(boolean debug) {
        showLevel = debug;
        showTimestamp = debug;
    }

    public static void setupLogging( boolean debugEnabled, List<String> debugPackages ) {
        final CLILogFormatter logFormatter = new CLILogFormatter();
        Logger logger = Logger.getLogger("");
        for (final Handler handler : logger.getHandlers()) {
            if (handler instanceof ConsoleHandler) {
                handler.setFormatter(logFormatter);
            }
        }
        logFormatter.setDebug(debugEnabled);
        if (debugEnabled) {
            for (final Handler handler : Logger.getLogger("").getHandlers()) {
                handler.setLevel(Level.FINEST);
            }
            for (String pkg : debugPackages) {
                Logger.getLogger(pkg).setLevel(Level.FINEST);
            }
        }
    }
}
