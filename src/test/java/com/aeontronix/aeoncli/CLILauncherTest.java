/*
 * Copyright (c) 2023. Aeontronix Inc
 */

package com.aeontronix.aeoncli;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CLILauncherTest {
    @Test
    void execute() {
        MainCmd mainCmd = new MainCmd();
        CLILauncher CLILauncher = new CLILauncher(mainCmd,"--debug","sc");
        Assertions.assertEquals(0, CLILauncher.execute());
    }
}
