/*
 * Copyright (c) 2023. Aeontronix Inc
 */

package com.aeontronix.aeoncli;

import picocli.CommandLine;

import java.util.concurrent.Callable;

@CommandLine.Command(name = "sc")
public class SubCmd implements Callable<Integer> {
    @Override
    public Integer call() throws Exception {
        System.out.println("Executed SubCmd");
        return 0;
    }
}
