# Introduction

This framework is used to create java command line projects

## Logging

It is assumed that java.util.logging will be used for all logging.

The framework will automatically reconfigure all appender to output a simpler logging
that is more appropriate for a CLI
